variable "location" {
  type = string
}

variable "resource_group_name" {
  type = string
}

variable "app_service_plan_name" {
  type = string
}

variable "app_service_name" {
  type = string
}

variable "my_key_vault" {
  type = string
}

variable "enabled_for_disk_encryption" {
  type = bool
}

variable "soft_delete_retention_days" {
  type = number
}

variable "purge_protection_enabled" {
  type = bool
}

variable "os_type" {
  type = string
}

variable "sku_tier" {
  type = string
}

variable "sku_size" {
  type = string
}

variable "sku_name" {
  type = string
}

variable "my_network_interface_name" {
  type = string
}

variable "network_security_group_name" {
  type = string
}

variable "public_ip_environment" {
  type = string
}

variable "public_ip_name" {
  type = string
}

variable "my_configuration" {
  type = string
}

variable "allocation_method" {
  type = string
}

variable "my_subnet_name" {
  type = string
}

variable "virtual_network_name" {
  type = string
}


variable "address_prefixes" {
  type = list(string)
}


variable "virtual_network_address_space" {
  type = list(string)
}

variable "private_ip_address_allocation" {
  type = string
}

variable "nsg_environment" {
  type = string
}

variable "vn_environment" {
  type = string
}

variable "vm_name" {
  type = string
}

variable "vm_size" {
  type = string
}