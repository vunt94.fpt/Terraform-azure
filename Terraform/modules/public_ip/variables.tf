variable "public_ip_name" {
  type = string
  description = ""
}

variable "location" {
  type = string
  description = ""
}

variable "resource_group_name" {
  type = string
  description = ""
}

variable "allocation_method" {
  type = string
  description = ""
}

variable "public_ip_environment" {
  type = string
  description = ""
}
