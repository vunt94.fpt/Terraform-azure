# Create public IPs
resource "azurerm_public_ip" "my_public_ip" {
    name                         = var.public_ip_name
    location                     = var.location
    resource_group_name          = var.resource_group_name
    allocation_method            = var.allocation_method

    tags = {
        environment = var.public_ip_environment
    }
}