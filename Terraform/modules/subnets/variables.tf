variable "virtual_network_name" {
  type = string
  description = "Virtual network name"
}

variable "resource_group_name" {
  type = string
  description = ""
}

variable "subnet_name" {
  type = string
  description = ""
}

variable "address_prefixes" {
  type = list(string)
  description = ""
}
