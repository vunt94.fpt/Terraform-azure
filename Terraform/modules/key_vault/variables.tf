variable "location" {
  type = string
  description = ""
}

variable "resource_group_name" {
  type = string
  description = ""
}

variable "enabled_for_disk_encryption" {
  type = bool
  description = ""
}

variable "purge_protection_enabled" {
  type = bool
  description = ""
}

variable "soft_delete_retention_days" {
  type = number
  description = ""
}

variable "sku_name" {
  type = string
  description = ""
}

variable "my_key_vault" {
  type = string
  description = ""
}