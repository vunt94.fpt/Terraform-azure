variable "virtual_network_name" {
  type = string
  description = "Virtual network name"
}

variable "virtual_network_address_space" {
  type = list(string)
  description = ""
}

variable "location" {
  type = string
  description = ""
}

variable "vn_environment" {
  type = string
  description = ""
}

variable "resource_group_name" {
  type = string
  description = ""
}
