variable "my_network_interface_name" {
  type = string
  description = ""
}

variable "location" {
  type = string
  description = ""
}

variable "resource_group_name" {
  type = string
  description = ""
}

variable "my_configuration" {
  type = string
  description = ""
}

variable "subnet_id" {
  type = string
  description = ""
}

variable "private_ip_address_allocation" {
  type = string
  description = ""
}

variable "public_ip_address_id" {
  type = string
  description = ""
}
