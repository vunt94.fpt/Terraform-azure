# Create network interface
resource "azurerm_network_interface" "my_network_interface" {
    name                      = var.my_network_interface_name
    location                  = var.location
    resource_group_name       = var.resource_group_name

    ip_configuration {
        name                          = var.my_configuration
        subnet_id                     = var.subnet_id
        private_ip_address_allocation = var.private_ip_address_allocation
        public_ip_address_id          = var.public_ip_address_id
    }
}