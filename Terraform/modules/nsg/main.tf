resource "azurerm_network_security_group" "my_network_security_group" {
  name                = var.network_security_group
  location            = var.location
  resource_group_name = var.resource_group_name
  tags = {
    environment = var.nsg_environment
  }
}

resource "azurerm_network_security_rule" "nsr_allow_access_from_internet" {
  name                        = "allowAccessFromInternet"
  priority                    = 201
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "Tcp"
  source_port_range           = "*"
  destination_port_range      = "80"
  source_address_prefix       = "Internet"
  description                 = ""
  destination_address_prefix  = "*"
  resource_group_name         = var.resource_group_name
  network_security_group_name = azurerm_network_security_group.my_network_security_group.name
}

resource "azurerm_network_security_rule" "nsr_allow_send_request_to_be" {
  name                        = "allowSendReqToBE"
  priority                    = 200
  direction                   = "Outbound"
  access                      = "Allow"
  protocol                    = "Tcp"
  source_port_range           = "*"
  destination_port_range      = "*"
  source_address_prefixes     = ["10.0.1.0/24"]
  description                 = ""
  destination_address_prefix  = "*"
  resource_group_name         = var.resource_group_name
  network_security_group_name = azurerm_network_security_group.my_network_security_group.name
}

resource "azurerm_network_security_rule" "nsr_allow_response_from_be" {
  name                        = "allowResponseFromBE"
  priority                    = 200
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "Tcp"
  source_port_range           = "*"
  destination_port_range      = "*"
  source_address_prefixes     = ["10.0.1.0/24"]
  description                 = ""
  destination_address_prefix  = "*"
  resource_group_name         = var.resource_group_name
  network_security_group_name = azurerm_network_security_group.my_network_security_group.name
}