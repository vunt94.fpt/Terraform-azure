variable "network_security_group" {
  type        = string
  description = ""
}

variable "location" {
  type        = string
  description = ""
}

variable "resource_group_name" {
  type        = string
  description = ""
}

variable "nsg_environment" {
  type        = string
  description = ""
}
