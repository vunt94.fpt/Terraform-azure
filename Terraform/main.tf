terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "3.49.0"
    }
  }
  backend "azurerm" {
    resource_group_name = "packer-rg"
    storage_account_name = "mystoragetfstate94"
    container_name = "mycontainer"
    key = "terraform.tfstate"
  }
}

provider "azurerm" {
  features {
    resource_group {
      prevent_deletion_if_contains_resources = false
    }
  }
}

module "resource_group" {
  source              = "./modules/resource_group"
  resource_group_name = var.resource_group_name
  location            = var.location
}

module "app_services" {
  source                = "./modules/app_services"
  app_service_plan_name = var.app_service_plan_name
  app_service_name      = var.app_service_name
  location              = module.resource_group.location
  resource_group_name   = module.resource_group.rg_name
  sku_tier              = var.sku_tier
  sku_size              = var.sku_size
}

module "key_vault" {
  source                      = "./modules/key_vault"
  my_key_vault                = var.my_key_vault
  location                    = module.resource_group.location
  resource_group_name         = module.resource_group.rg_name
  enabled_for_disk_encryption = var.enabled_for_disk_encryption
  soft_delete_retention_days  = var.soft_delete_retention_days
  purge_protection_enabled    = var.purge_protection_enabled
  sku_name                    = var.sku_name
}

module "public_ip" {
  source                = "./modules/public_ip"
  public_ip_name        = var.public_ip_name
  location              = module.resource_group.location
  resource_group_name   = module.resource_group.rg_name
  allocation_method     = var.allocation_method
  public_ip_environment = var.public_ip_environment
}

module "subnets" {
  source               = "./modules/subnets"
  subnet_name          = var.my_subnet_name
  resource_group_name  = module.resource_group.rg_name
  virtual_network_name = module.virtual_network.virtual_network_name
  address_prefixes     = var.address_prefixes
}

module "network_interface" {
  source                        = "./modules/network_interface"
  my_network_interface_name     = var.my_network_interface_name
  location                      = module.resource_group.location
  my_configuration              = var.my_configuration
  resource_group_name           = module.resource_group.rg_name
  public_ip_address_id          = module.public_ip.public_ip_id
  subnet_id                     = module.subnets.subnet_id
  private_ip_address_allocation = var.private_ip_address_allocation
}

module "nsg" {
  source                 = "./modules/nsg"
  network_security_group = var.network_security_group_name
  location               = module.resource_group.location
  resource_group_name    = module.resource_group.rg_name
  nsg_environment        = var.nsg_environment
}

module "virtual_network" {
  source                        = "./modules/virtual_network"
  virtual_network_name          = var.virtual_network_name
  virtual_network_address_space = var.virtual_network_address_space
  location                      = module.resource_group.location
  resource_group_name           = module.resource_group.rg_name
  vn_environment                = var.vn_environment
}

module "vm" {
  source                = "./modules/vm"
  vm_name               = var.vm_name
  location              = module.resource_group.location
  resource_group_name   = module.resource_group.rg_name
  network_interface_ids = [module.network_interface.network_interface_id]
  vm_size               = var.vm_size
}