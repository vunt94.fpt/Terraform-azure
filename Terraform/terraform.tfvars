location                      = "East US"
resource_group_name           = "azure-project-rg"
my_key_vault                  = "vunt94-key-vault"
app_service_plan_name         = "my-app-service-plan-name"
app_service_name              = "my-app-service-name"
enabled_for_disk_encryption   = true
soft_delete_retention_days    = 7
purge_protection_enabled      = false
os_type                       = "Linux"
sku_tier                      = "Standard"
sku_size                      = "S1"
sku_name                      = "standard"
my_network_interface_name     = "my-network-interface"
network_security_group_name   = "my-nsg"
public_ip_environment         = "Production"
my_configuration              = "my-ni-configuration"
public_ip_name                = "my-public-ip"
allocation_method             = "Dynamic"
my_subnet_name                = "my-subnet"
virtual_network_name          = "my-vn"
address_prefixes              = ["10.0.1.0/24"]
virtual_network_address_space = ["10.0.0.0/16"]
private_ip_address_allocation = "Dynamic"
nsg_environment               = "Production"
vn_environment                = "Production"
vm_name                       = "my_vm"
vm_size                       = "Standard_DS1_v2"
# test